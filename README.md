Vagrant Demo
============

Vagrant VM management system prototype

Dependencies
------------

### OS X
``` sh
brew install Caskroom/cask/vagrant npm python
pip install --upgrade Django djangorestframework PyGithub python-social-auth
```

### Fedora
```
sudo yum -y install git make npm python-django python-django-rest-framework python-PyGithub python-social-auth vagrant
```

Installation
------------

### All platforms
``` sh
git clone https://bitbucket.org/jh86/vagrant-api
cd vagrant-api
make
```

Open web browser to http://127.0.0.1:8000/static/dashboard.html
