MAKE_BAK := $(MAKE)
MAKE = $(MAKE_BAK) --no-print-directory

combined: backend frontend

pip_reqs:
	pip install --requirement requirements.txt

backend:
	@$(MAKE) -C $@

frontend:
	@$(MAKE) -C $@

clean:
	@$(MAKE) -C backend clean
	@$(MAKE) -C frontend clean

all: combined

.PHONY: combined pip_reqs backend frontend clean
