'use strict';

/* App Module */

var phonecatApp = angular.module('phonecatApp', [
  'ngRoute',
  'phonecatAnimations',

  'phonecatControllers',
  'phonecatFilters',
  'phonecatServices'
]);

phonecatApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      // when('/phones', {
      //   templateUrl: 'partials/phone-list.html',
      //   controller: 'PhoneListCtrl'
      // }).
      // when('/phones/:phoneId', {
      //   templateUrl: 'partials/phone-detail.html',
      //   controller: 'PhoneDetailCtrl'
      // }).
      when('/instances', {
        templateUrl: 'partials/instance-list.html',
        controller: 'InstanceListCtrl'
      }).
      when('/instances/:instanceId', {
        templateUrl: 'partials/instance-detail.html',
        controller: 'InstanceDetailCtrl'
      }).
      otherwise({
        redirectTo: '/instances'
      });
  }]);
