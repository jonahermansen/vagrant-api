'use strict';

/* Controllers */

var phonecatControllers = angular.module('phonecatControllers', []);

phonecatControllers.controller(
    'PhoneListCtrl',
    [
	'$scope',
	'Phone',
	function($scope, Phone) {
	    $scope.phones = Phone.query();
	    $scope.orderProp = 'age';
	}
    ]
);

phonecatControllers.controller(
    'PhoneDetailCtrl',
    [
	'$scope',
	'$routeParams',
	'Phone',
	function($scope, $routeParams, Phone) {
	    $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone) {
		$scope.mainImageUrl = phone.images[0];
	    });

	    $scope.setImage = function(imageUrl) {
		$scope.mainImageUrl = imageUrl;
	    };
	}
    ]
);

phonecatControllers.controller(
    'InstanceListCtrl',
    [
	'$scope',
	'Instance',
	function($scope, Instance) {
	    $scope.instances = Instance.query();
	    // $scope.orderProp = 'age';
	    console.log('got here 2');
	    // $('#instance_list').DataTable();
	    //$('#instance_list').DataTable();
	}
    ]
);

phonecatControllers.controller(
    'InstanceDetailCtrl',
    [
	'$scope',
	'$routeParams',
	'Instance',
	function($scope, $routeParams, Instance) {
	    console.log('got here 3');
	    $scope.instance = Instance.get({instanceId: 1}, function(instance) {
		console.log('got here 4');

		// $scope.mainImageUrl = instance.images[0];
	    });

	    // $scope.setImage = function(imageUrl) {
	    // 	$scope.mainImageUrl = imageUrl;
	    // };
	}
    ]
);
