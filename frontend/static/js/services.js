'use strict';

/* Services */

var phonecatServices = angular.module('phonecatServices', ['ngResource']);

phonecatServices.factory(
    'Phone',
    [
	'$resource',
	function($resource) {
	    return $resource('phones/:phoneId.json', {}, {
		query: { method: 'GET', params: { phoneId: 'phones' }, isArray: true }
	    });
	}
    ]
);

phonecatServices.factory(
    'Instance',
    [
	'$resource',
	function($resource) {
	    // console.log('got here');
	    // console.log(arguments);
	    return $resource('/demo/instances/:instanceId', {}, {
		query: { method: 'GET', params: { instanceId: 'instances' }, isArray: true },
		get: { method: 'GET', params: { instanceId: 'instances' }, isArray: false },
	    });
	}
    ]
);

// phonecatServices.factory(
//     'Instance',
//     [
// 	'$http',
// 	function($http) {
// 	    console.log('here');
// 	    return {
// 		query: function () {
// 		    return $http.get(
// 			'/demo/instances', {
// 			    transformResponse: function (data, headers) {
// 				// data = JSON.parse(data);
// 				console.log(data);
// 				// return data.instances;
// 				return data;
// 			    }
// 			}
// 		    );
// 		}
// 	    };
// 	}
//     ]
// );
