from django.conf.urls import url
#from django.conf import settings

from django.views.generic import TemplateView
from . import views

urlpatterns = [
    #url(r'^$', 'django.contrib.staticfiles.views.serve', kwargs={'path': 'dashboard.html', 'document_root': settings.STATIC_ROOT}),
    # url(r'^$', views.index, name='index'),
    url(r'^$', TemplateView.as_view(template_name='dashboard.html')),
    url(r'^branches$', views.branches, name='branches'),
    url(r'^tags$', views.tags, name='tags'),
    url(r'^instances/$', views.instances, name='instances'),
    # url(r'^instances', views.instances, name='instances'),
    url(r'^instances/(?P<instance_id>\w{1,50})/$', views.instance, name='instance'),
    url(r'^users$', views.users, name='users'),
    url(r'^hosts$', views.hosts, name='hosts'),
]
