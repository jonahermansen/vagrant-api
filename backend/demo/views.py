# from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse, JsonResponse
from github import Github
import json
from . import models

git_token = 'a246d36a032bd6052b9ef23498171ae5349f48e3'
github = Github(base_url='https://git.edgecastcdn.net/api/v3', login_or_token=git_token)

def index(request):
    return HttpResponse("Hello, world. You're at the demo index.")

def branches(request):
    branch_list = []
    for branch in github.get_user('Portals').get_repo('Portals').get_branches():
        branch_list.append(branch.name)
    return JsonResponse({'branches': branch_list})

def tags(request):
    tag_list = []
    for tag in github.get_user('Portals').get_repo('Portals').get_tags():
        tag_list.append("%s-%s" % (tag.name, tag.commit.sha))
    return JsonResponse({'tags': tag_list})

# TODO: return a generic instance
def instances(request):
    instances = map((lambda x: x.__json__()), models.PortalsVagrantInstance.objects.all())
    return JsonResponse(instances, safe=False)

def instance(request, instance_id):
    if instance_id == 'instances':
        return instances(request)
    else:
        my_instances = map((lambda x: x.__json__()), models.PortalsVagrantInstance.objects.filter(id=instance_id))
        return JsonResponse(my_instances[0])

def users(request):
    usernames = map((lambda x: x.username), models.User.objects.all())
    return JsonResponse({'users': usernames})

def hosts(request):
    hosts = map((lambda x: x.__json__()), models.VagrantHost.objects.all())
    return JsonResponse({'hosts': hosts})

def json_response(obj):
    return HttpResponse(json.dumps(obj))
