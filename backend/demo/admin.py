from django.contrib import admin
from .models import *

# Register your models here.
@admin.register(VagrantPlatform)
class VagrantPlatformAdmin(admin.ModelAdmin):
   pass

@admin.register(VagrantHost)
class VagrantHostAdmin(admin.ModelAdmin):
   pass

@admin.register(VagrantProvider)
class VagrantProviderAdmin(admin.ModelAdmin):
   pass

@admin.register(VagrantHostProvider)
class VagrantHostProviderAdmin(admin.ModelAdmin):
   pass

@admin.register(VagrantInstance)
class VagrantInstanceAdmin(admin.ModelAdmin):
   pass

@admin.register(PortalsVagrantInstance)
class VagrantPortalsVagrantInstanceAdmin(admin.ModelAdmin):
   pass
