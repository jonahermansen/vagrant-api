from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
#from django.contrib import admin

from django.utils.dateformat import format

# Create your models here.

# TODO: disable multiple vagrant hosts?
class VagrantPlatform(models.Model):
    def __str__(self):
        return self.platform

    platform_choices = (
        ('darwin', 'OS X'),
        ('windows', 'Windows'),
        ('linux', 'Linux')
    )
    name = models.CharField(max_length=7, choices=platform_choices)
    enabled = models.BooleanField()

class VagrantProvider(models.Model):
    def __str__(self):
        return self.name

    # TODO: test vmware_fusion, parallels_desktop, hyperv, azure, aws
    provider_choices = (
        ('virtualbox', 'VirtualBox'), # native support
        ('vmware_fusion', 'VMware Fusion'), # sponsored plug-in
        ('parallels_desktop', 'Parallels Desktop'), # vendor-provided
        ('hyperv', 'Microsoft Hyper-V'), # native support
        ('azure', 'Microsoft Azure'), # vendor-provided
        ('aws', 'Amazon Web Services'), # TODO: native support?
        ('libvirt', 'libvirt') # third-party plugin
    )
    name = models.CharField(max_length=17, choices=provider_choices)
    enabled = models.BooleanField()

class VagrantHostProvider(models.Model):
    def __str__(self):
        return "%s-%s" % (self.host, self.provider)

    # 'host' ForeignKey declared as a string to avoid circular schema references
    host = models.ForeignKey('VagrantHost')
    provider = models.ForeignKey(VagrantProvider)
    enabled = models.BooleanField()

class VagrantHost(models.Model):
    def __str__(self):
        return self.fqdn

    def __json__(self):
        return {
            self.fqdn: {
                "ctime": int(format(self.ctime, 'U')),
                "enabled": self.enabled,
                "id": self.id,
                "platform": self.platform.name,
                "providers": map((lambda x: x.provider.name), VagrantHostProvider.objects.filter(host_id=self.id, enabled=True))
            }
        }

    ctime = models.DateTimeField('date created')
    fqdn = models.CharField(max_length=255, verbose_name='FQDN')
    platform = models.ForeignKey(VagrantPlatform)
    enabled = models.BooleanField()


# TODO: prevent this table from being created?
class VagrantInstance(models.Model):
    def __str__(self):
        return "%s-%s-%s" % (self.creator.username, self.host, self.provider.name)

    def __json__(self):
        return {
            "creator": self.creator.username,
            "ctime": int(format(self.ctime, 'U')),
            "host": self.host.fqdn,
            "id": self.id,
            "mem_limit": str(self.mem_limit / 1024**3) + ' GiB',
            "num_cpus": self.num_cpus,
            "provider": self.provider.name,
            "status": self.status,
        }

    # dependencies
    host = models.ForeignKey(VagrantHost)
    provider = models.ForeignKey(VagrantProvider)
    creator = models.ForeignKey(User)

    # metadata
    ctime = models.DateTimeField('date created')
    num_cpus = models.PositiveSmallIntegerField()
    mem_limit = models.PositiveIntegerField() # bytes

    # state
    status_choices = (
        ('up', 'Up'),
        ('halted', 'Halted'),
        ('suspended', 'Suspended'),
        ('destroyed', 'Destroyed'),
    )
    status = models.CharField(max_length=9, choices=status_choices)

# child class of VagrantInstance which also stores the Portals branch we used
class PortalsVagrantInstance(VagrantInstance):
    def __str__(self):
        return "portals-%s-%s-%s-%s" % (self.creator.username, self.host, self.provider, self.branch)

    branch = models.CharField(max_length=255) # TODO: github branch max chars?
